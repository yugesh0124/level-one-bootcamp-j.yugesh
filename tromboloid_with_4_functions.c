//WAP to find the volume of a tromboloid using 4 functions.

#include <stdio.h>
float input()
{
    float a;
    
    printf("Enter Number:");
    scanf("%f",&a);
    
    return a;
}
float find_vol(float a,float b,float c)
{
    float vol;
    vol=((a*b*c)+b/c)/3;
    
    
    return vol;
}
void output(float a,float b,float c,float vol)
{
    printf("Volume of tromboloid with h=%f,d=%f and b=%f is %f",a,b,c,vol);
    
}
int main()
{
    float h,d,b,v;
    
    h=input();
    d=input();
    b=input();
    v=find_vol(h,d,b);
    output(h,d,b,v);
    
    return 0;
}
