#include<stdio.h>
typedef  struct
{
Int n;
Int d;
}
Fraction;
fractions input()
{
fractions f;
printf("Enter the numerator and denominator:  ");
scanf("%d %d",&f.n,&f.d);
return f;
}
int hcf(fractions f)
{
int i,gcd;
for(i=1;i<=f.n && i<=f.d;i++)
{
if(f.n%i==0 && f.d%i==0)
gcd=i;
}
return gcd;
}

fractions sum(fractions f1,fractions f2)
{
int gcd;
fractions r;
r.n=(f1.n*f2.d)+(f2.n*f1.d);
r.d=f1.d*f2.d;
gcd=hcf(r);
r.n=r.n/gcd;
r.d=r.d/gcd;
return r;
}

void result(fractions f1,fractions f2,fractions r)
{
printf("The sum of %d/%d + %d/%d is %d/%d",f1.n,f1.d,f2.n,f2.d,r.n,r.d);
}
int main()
{
fractions f1,f2,res;
printf("Enter the numerator and denominator of the first fraction: ");
f1=input();
printf("Enter the numerator and denominator of the second fraction: ");
f2=input();
res=sum(f1,f2);
result(f1,f2,res);
return 0;
}
