//WAP to find the sum of n fractions.
#include<stdio.h>
typedef struct fraction
{
    int num;
    int deno;
}Fraction;
int input_n()
{
    int n;
    printf("Enter the number of fractions to be added:");
    scanf("%d",&n);
    return n;
}
Fraction input_fraction(int n,Fraction a[n])
{
	for(int i=0;i<n;++i)
	{
	printf("Enter the fraction %d (numerator and denominator):",i+1);
	scanf("%d%d",&a[i].num,&a[i].deno);
	}
}
int hcf(int a,int b)
{
    int gcd;
	for(int i=1;i<=a && i<=b;i++)
	{
	    if(a%i==0 && b%i==0)
	        {
	            gcd=i;
	        }
	}
	return gcd;
}
Fraction sum_of_2_fraction(Fraction f1,Fraction f2)
{
    Fraction sum;
    sum.num=f1.num*f2.deno+f2.deno*f1.num;
    sum.deno=f1.deno*f2.deno;
    return sum;
}
Fraction sum_of_n_fractions(int n,Fraction a[])
{
    Fraction r;
    int gcd;
    int numerator=0,denominator=1;
    for(int i=0;i<n;i++)
    {
        denominator *= a[i].deno;
    }
    for(int i=0;i<n;i++)
    {
        numerator += (a[i].num)*(denominator/a[i].deno);
    }
    Fraction add = {numerator,denominator};
    gcd=hcf(add.num,add.deno);
    r.num=add.num/gcd;
    r.deno=add.deno/gcd;
	return r;
}
void result(int n,Fraction a[n],Fraction r)
{
	int i;
	printf("The sum of ");
	for(i=0;i<n-1;++i){
	    printf("%d/%d+",a[i].num,a[i].deno);
	}
	printf("%d/%d=%d/%d",a[i].num,a[i].deno,r.num,r.deno);
}
int main()
{
	int n;
	Fraction fraction_result;
	Fraction a[n];
	n=input_n();
	input_fraction(n,a);
	fraction_result=sum_of_n_fractions(n,a);
	result(n,a,fraction_result);
	return 0;
}