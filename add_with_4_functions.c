//Write a program to add two user input numbers using 4 functions.

#include <stdio.h>
int input()
{
    int a;
    
    printf("Enter Number:");
    scanf("%d",&a);
    
    return a;
}
int find_sum(int a,int b)
{
    int sum;
    sum=a+b;
    
    return sum;
}
void output(int a,int b,int sum)
{
    printf("Sum of %d and %d is %d",a,b,sum);
    
}
int main()
{
    int x,y,s;
    
    x=input();
    y=input();
    s=find_sum(x,y);
    output(x,y,s);
    
    return 0;
}